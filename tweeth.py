#!/usr/bin/python
import tweepy
import csv 
auth = tweepy.auth.OAuthHandler('auth token', 'auth secret')
auth.set_access_token('key', 'key secret')

api = tweepy.API(auth)
# Open/Create a file to append data
csvFile = open('hyd.csv', 'w')
#Use csv Writer
csvWriter = csv.writer(csvFile)

for tweet in tweepy.Cursor(api.search, 
                    q="@hydcitytraffic", 
                    since="2016-06-21", 
                    until="2016-06-29", 
                    lang="en").items():
    #Write a row to the csv file/ I use encode utf-8
    csvWriter.writerow([tweet.created_at, tweet.text.encode('utf-8',errors='strict')])
    print tweet.created_at, tweet.text.encode(encoding='utf-8',errors='strict')
csvFile.close()
