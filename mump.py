import matplotlib.pyplot as plt
import pandas as pd

data = pd.read_csv('/home/aastha9/mumf.csv', sep=',',header=None, index_col =0)

plt.hist(data["ACT"])
plt.ylabel('Tweet Count')
plt.xlabel('Areas')
plt.title('Mumbai areas and traffic tweets')

plt.show()
