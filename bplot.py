import pandas
import matplotlib.pyplot as plt

data=pandas.read_csv('/home/aastha9/mumf.csv', sep=',')
fig=plt.figure()
ax = fig.add_subplot(3,2,3)
ax.hist(data['ACT'],bins=21)
plt.title('Mumbai Areas and Traffic Distribution')
plt.xlabel('Areas')
plt.ylabel('ACT')
plt.show()




